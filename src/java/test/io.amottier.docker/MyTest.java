package io.amottier.docker;

import org.junit.Rule;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.testcontainers.containers.MariaDBContainer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MyTest {
    @Rule
    public MariaDBContainer mariaDB = new MariaDBContainer();

    @Test
    public void testA() throws SQLException {
        String url = mariaDB.getJdbcUrl();
        Connection connection = DriverManager.getConnection(url);
        connection.createStatement().executeQuery("SELECT * FROM toto;");
    }
}